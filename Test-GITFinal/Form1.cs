﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test_GITFinal
{
    public partial class Form1 : Form
    {
        List<string> list = new List<string>();
        public Form1()
        {
            InitializeComponent();
            list.Add("Sup0");
            list.Add("Sup1");
            list.Add("Sup2");
            list.Add("Sup3");
            list.Add("Sup4");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //hello
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (var s in list)
            {
                listBox1.Items.Add(s);
                int index = list.IndexOf(s);
                listBox1.Items.Add(index.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            list.RemoveAt(1);
            foreach (var s in list)
            {
                listBox2.Items.Add(s);
                int index = list.IndexOf(s);
                listBox2.Items.Add(index.ToString());
            }
        }
    }
}
